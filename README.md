# My simple Todolist REST API frontend.

## Prerequisites

Make sure you have the following installed on your machine before you begin:

- **Node.js**
- **Yarn** ( your favourite node package manager should work )

this project is tested on node **v20.11.0** and yarn **v1.22.21**

# Getting started

First navigate to the project root and run these two commands on the terminal:

```bash
$ yarn install
$ yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
