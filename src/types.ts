import { UnknownAction } from "redux";

export type Todo = {
  id: number;
  title: string;
  status: Status;
};

export enum Status {
  Completed = "completed",
  Pending = "pending",
}

export interface ITodoAction extends UnknownAction {
  type: TODO_ACTIONS;
  payload?: any;
}

export enum TODO_ACTIONS {
  FETCH_TODOS_REQUEST = "FETCH_TODOS_REQUEST",
  ADD_TODO = "ADD_TODO",
  REMOVE_TODO = "REMOVE_TODO",
  UPDATE_TODO = "UPDATE_TODO",
}
