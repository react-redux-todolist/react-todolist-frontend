import { ITodoAction, Status, TODO_ACTIONS } from "../../types";
import { immediateToast } from "izitoast-react";
import type { Dispatch } from "redux";
import "izitoast-react/dist/iziToast.css";

const apiEndpoint = process.env.REACT_APP_API_ENDPOINT;

export const fetchTodos = () => {
  return async (dispatch: Dispatch<ITodoAction>) => {
    fetch(`${apiEndpoint}/`)
      .then((data) => data.json())
      .then((response) => {
        dispatch({
          type: TODO_ACTIONS.FETCH_TODOS_REQUEST,
          payload: response,
        });
      })
      .catch((error) => {
        // izitoast is a beautiful way to show the user a message
        immediateToast("warning", {
          message: error.message,
        });
      });
  };
};

export const addTodo = (data: { title: string }) => {
  return async (dispatch: Dispatch<ITodoAction>) => {
    fetch(`${apiEndpoint}/`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...data, status: Status.Pending }),
    })
      .then((data) => data.json())
      .then((response) => {
        dispatch({
          type: TODO_ACTIONS.ADD_TODO,
          payload: response,
        });
        immediateToast("success", {
          message: "Addition Succesfull",
        });
      })
      .catch((error) => {
        immediateToast("warning", {
          message: error.message,
        });
      });
  };
};

export const updateTodo = (id: number, status: Status) => {
  return async (dispatch: Dispatch<ITodoAction>) => {
    fetch(`${apiEndpoint}/${id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ status: status }),
    })
      .then((data) => data.json())
      .then((response) => {
        dispatch({
          type: TODO_ACTIONS.UPDATE_TODO,
          payload: response,
        });
        immediateToast("success", {
          message: "Update Succesfull",
        });
      })
      .catch((error) => {
        immediateToast("warning", {
          message: error.message,
        });
      });
  };
};

export const removeTodo = (todoId: string) => {
  return async (dispatch: Dispatch<ITodoAction>) => {
    fetch(`${apiEndpoint}/${todoId}`, {
      method: "DELETE",
    })
      .then((data) => data.json())
      .then((response) => {
        immediateToast("success", {
          message: "Removal Succesfull",
        });
        dispatch({
          type: TODO_ACTIONS.REMOVE_TODO,
          payload: response,
        });
      })
      .catch((error) => {
        immediateToast("warning", {
          message: error.message,
        });
      });
  };
};
