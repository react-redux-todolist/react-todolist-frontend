import { ITodoAction, TODO_ACTIONS, Todo } from "../../types";
import type { Reducer, UnknownAction } from "redux";

const todosReducer: Reducer<Todo[], ITodoAction | UnknownAction, Todo[]> =
  function todosReducer(
    state: Todo[] = [],
    action: ITodoAction | UnknownAction
  ): Todo[] {
    switch (action.type) {
      case TODO_ACTIONS.FETCH_TODOS_REQUEST: {
        return action.payload;
      }
      case TODO_ACTIONS.ADD_TODO: {
        return state.concat(action.payload);
      }
      case TODO_ACTIONS.UPDATE_TODO: {
        return state.map((todo) =>
          todo.id === action.payload.id ? action.payload : todo
        );
      }
      case TODO_ACTIONS.REMOVE_TODO: {
        return state.filter((todo) => todo.id !== action.payload.id);
      }
      default:
        return state;
    }
  };

export { todosReducer };
