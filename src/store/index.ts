import { configureStore } from "@reduxjs/toolkit";

import { todosReducer } from "./reducers/todolistReducer";

const store = configureStore({
  reducer: {
    todolist: todosReducer,
  },
});

export default store;
