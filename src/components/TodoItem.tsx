import styled from "styled-components";
import { MdDelete } from "react-icons/md";

import { removeTodo, updateTodo } from "../store/actions/todolsitActions";
import { useDispatch } from "react-redux";

import { Status, type Todo } from "../types";
import { UnknownAction } from "redux";

const TodoItemWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;
  margin: 4px 0;
  border: 1px solid #ccc;
  border-radius: 5px;
  position: relative;
  align-items: center;
  font-weight: bold;

  &:hover {
    background-color: #f7f7f7;
  }

  button {
    background-color: #e99c9c;
    border-radius: 5px;
    padding: 5px 10px;

    svg {
      scale: 1.5;
    }

    &:hover {
      cursor: pointer;
    }
  }

  span.todo-title {
    width: 250px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  //syblings selector to target elements at the same level
  &.completed {
    opacity: 0.8;
    font-weight: normal;
    font-style: italic;

    // this strike through the completed todos
    &:after {
      content: "";
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      width: 80%;
      height: 1px;
      background: black;
    }

    input[type="checkbox"]:checked {
      accent-color: gray;
    }
  }
`;

interface props {
  todo: Todo;
  index: number;
}

export const TodoItem: React.FunctionComponent<props> = ({ todo, index }) => {
  const dispatch = useDispatch();
  return (
    <TodoItemWrapper
      key={todo.id}
      // adding a completed class to the todo item will help targeting completed todos for styling
      className={todo.status === "completed" ? "completed" : ""}
    >
      {/*
        it is not needed to show the todo id for the end user unless for debugging,
        using the item place in the list instead of the todo id
      */}
      {/* <span> {todo.id}</span> */}
      <span> {index + 1}</span>

      <input
        type="checkbox"
        name="scales"
        checked={todo.status === Status.Completed}
        onChange={(event) => {
          const target = event.target;
          dispatch(
            updateTodo(
              todo.id,
              target.checked ? Status.Completed : Status.Pending
            ) as unknown as UnknownAction
          );
        }}
      />
      <span className="todo-title">{todo.title}</span>
      {/*
        since check box itself is representing the task status,
        commenting the following span tag will give more room for title field 
      */}
      {/* <span>{todo.status}</span> */}
      <button
        onClick={() => {
          dispatch(removeTodo(todo.id.toString()) as unknown as UnknownAction);
        }}
      >
        <MdDelete />
      </button>
    </TodoItemWrapper>
  );
};
