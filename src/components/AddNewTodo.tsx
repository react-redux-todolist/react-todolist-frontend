import { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import { addTodo } from "../store/actions/todolsitActions";
import { UnknownAction } from "redux";

const AddNewTodoWrapper = styled.div`
  margin-top: 20px;
  display: flex;
  width: 100%;

  input {
    padding: 10px;
    border-radius: 5px;
    border-color: snow;
    flex-grow: 1;
  }

  button {
    padding: 10px 20px;
    margin-left: 10px;
    border-radius: 5px;
    border-color: snow;
    border-color: darkgray;
    flex-grow: 1;
  }
`;

export const AddNewTodo = () => {
  const dispatch = useDispatch();
  const [newTodo, setNewTodo] = useState<string>("");

  return (
    <AddNewTodoWrapper>
      <input
        type="input"
        name={"new-todo"}
        value={newTodo}
        onChange={(event) => {
          setNewTodo(event.target.value);
        }}
        placeholder="Add new todo"
        onKeyDown={(event) => {
          if (event.key === "Enter") {
            dispatch(addTodo({ title: newTodo }) as unknown as UnknownAction);
            setNewTodo("");
          }
        }}
      ></input>
      <button
        disabled={newTodo === ""}
        onClick={() => {
          dispatch(addTodo({ title: newTodo }) as unknown as UnknownAction);
        }}
      >
        Add
      </button>
    </AddNewTodoWrapper>
  );
};
