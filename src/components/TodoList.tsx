import { useMemo, useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import debounce from "lodash.debounce";

import { TodoItem } from "./TodoItem";
import { Todo } from "../types";

const TodoListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 400px;
  padding: 20px;
  border: 1px solid #ccc;
  background-color: #dfdfdf;
  border-radius: 5px;
  overflow: auto;

  input[type="input"] {
    margin: 5px;
    padding: 10px;
    border-radius: 5px;
    border-color: snow;
  }

  div.item-list {
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 300px;
  }

  div.empty-indicator {
    display: flex;
    justify-content: center;
    margin-top: 20px;
    height: 100%;
    text-align: center;
  }

  input[type="checkbox"] {
    scale: 1.8;
  }
`;

export const TodoList = () => {
  const [filter, setFilter] = useState<string>("");

  const todoList = useSelector((state: any) => {
    return state.todolist;
  });

  // invoking debounce function will prevent the function from being called multiple times in a short period of time
  const debouncedSetFilter = useMemo(
    () => debounce((newValue) => setFilter(newValue), 300),
    []
  );

  // memorizing the filterd values will help in preventing the filter function to be called multiple times unless the filter field changes
  const filteredTodos = useMemo(() => {
    return todoList.filter((todo: Todo) => {
      return todo.title.toLowerCase().includes(filter.toLowerCase());
    });
  }, [todoList, filter]);

  return (
    <TodoListWrapper>
      <input
        name="filter"
        type="input"
        placeholder="Filter"
        disabled={todoList.length === 0}
        onChange={(event) => {
          debouncedSetFilter(event.target.value);
        }}
      ></input>
      <div className="item-list">
        {filter === "" && filteredTodos.length === 0 && (
          <div className="empty-indicator">
            Welcome to Todolist, <br /> Add a new todo to begin with
          </div>
        )}
        {filteredTodos.map((todo: Todo, index: number) => {
          return <TodoItem todo={todo} key={todo.id} index={index}></TodoItem>;
        })}
      </div>
    </TodoListWrapper>
  );
};
