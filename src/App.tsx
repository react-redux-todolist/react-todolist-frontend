import { useEffect } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import { AddNewTodo } from "./components/AddNewTodo";
import { TodoList } from "./components/TodoList";

import { fetchTodos } from "./store/actions/todolsitActions";
import { UnknownAction } from "redux";

const AppContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
`;

const AppWrapper = styled.div`
  display: flex;
  max-width: 600px;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border: 2px solid black;
  border-radius: 5px;
`;

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTodos() as unknown as UnknownAction);
  }, [dispatch]);

  return (
    <AppContainer>
      <AppWrapper>
        <header className="App-header">
          <h1>My Simple Todolist</h1>
        </header>
        <TodoList />
        <AddNewTodo />
      </AppWrapper>
    </AppContainer>
  );
}

export default App;
